import logging
logging.getLogger("scapy.runtime").setLevel(logging.ERROR)

from geoip2 import database
from scapy.all import *
from hashlib import md5
from time import strftime

# Get the geo-ip database.
READER = database.Reader("Geo.mmdb")


def geo_ip(ip):
    """
    Return the place to which the given ip is associated.
    """
    if ip.startswith("127.") or ip.startswith("192.168.") or ip.startswith("10."):
        return "Local"
    else:
        return READER.country(ip).country.name


def send(args):
    if len(args) == 8:
        # Format a "conversation started" message.
        txt = '{}|{}:{}|{}|{}:{}|{}|{}'.format(*args)

    elif len(args) == 3:
        # Format an "update values" message.
        txt = '{}|{}|{}'.format(*args)

    elif len(args) == 2:
        # Format a "conversation ended" message.
        txt = '{}|{}'.format(*args)
    # Send the formated message to the GUI.
    print(txt)


class Conversation:
    def __init__(self, hid, src_ip, src_port, dst_ip, dst_port, cur_time):
        self.src_ip = src_ip
        self.src_port = src_port
        self.dst_ip = dst_ip
        self.dst_port = dst_port
        self.country_dst = geo_ip(self.dst_ip)
        self.country_src = geo_ip(self.src_ip)

        self.start_time = cur_time
        self.end_time = None

        self.hash_id = md5(''.join([hid, cur_time])).hexdigest()
        self.inbound = 0
        self.outbound = 0
        self.last_ack = 0

    def check_end(self):
        # Check if the conversation has ended
        if self.last_ack and (self.end_time is not None):
            send((self.hash_id, self.end_time))
            return True
        return False

    def handle_pkt(self, pkt):
        # Update the conversations inbound/outbound values, and send it to the GUI.
        if pkt[IP].dst == self.src_ip:
            self.inbound += len(pkt)
        else:
            self.outbound += len(pkt)

        send((self.hash_id, self.inbound, self.outbound))

        # Check if received FIN/ACK, if so, expect to receive only one more ACK.
        if (pkt[TCP].flags & 0x01) and (pkt[TCP].flags & 0x10):
            self.last_ack = 1

        # Handle ACK/RST after FIN/ACK.
        elif self.last_ack and ((pkt[TCP].flags & 0x10) or (pkt[TCP].flags & 0x04)):
            self.end_time = strftime("%H:%M:%S %d/%m/%Y")

        # Handle RST.
        elif pkt[TCP].flags & 0x04:
            self.last_ack = 1
            self.end_time = strftime("%H:%M:%S %d/%m/%Y")


class Sniffer:
    def __init__(self):
        # Dictionary of ongoing conversations ({hash:conversation}).
        self.conversations = {}

        # Start sniffing and handle sniffed packets.
        self.sniff_pkt()

    def sniff_pkt(self):
        # Start sniffing packets (only capture tcp packets).
        # Handle them using handle_pkt.
        sniff(store=0, filter="tcp", prn=self.handle_pkt)

    def handle_pkt(self, pkt):
        src_ip = pkt[IP].src
        dst_ip = pkt[IP].dst
        src_port = str(pkt[TCP].sport)
        dst_port = str(pkt[TCP].dport)

        # Generate a unique MD5 hash id for the conversation.
        str_list = ''.join(sorted([src_ip, dst_ip, src_port, dst_port]))
        hash_id = md5(str_list).hexdigest()

        # Check if the packet is NOT related to an existing conversation.
        if hash_id not in self.conversations:
            # If packet isn't RST Packet, create a new conversation object.
            if not pkt[TCP].flags & 0x04:
                # Save the start time of the conversation.
                cur_time = strftime("%H:%M:%S %d/%m/%Y")
                self.conversations[hash_id] = Conversation(hash_id, src_ip, src_port,
                                                           dst_ip, dst_port, cur_time)

                send((self.conversations[hash_id].hash_id, self.conversations[hash_id].src_ip,
                      self.conversations[hash_id].src_port, self.conversations[hash_id].country_src,
                      self.conversations[hash_id].dst_ip, self.conversations[hash_id].dst_port,
                      self.conversations[hash_id].country_dst, self.conversations[hash_id].start_time))
        else:
            # If the packet is related to an existing connection, 
            # handle the packet using the existing conversation object.
            self.conversations[hash_id].handle_pkt(pkt)
            if self.conversations[hash_id].check_end():
                # If the conversation has ended, remove the conversation from the open conversations dictionary.
                del self.conversations[hash_id]


# Start Program.
try:
    main_sniffer = Sniffer()

except KeyboardInterrupt:
    READER.close()
    sys.exit()

except IOError:
    # If parent process has closed, exit the program.
    sys.exit()
