import sys
import os
import subprocess
import shlex

country_to_cc = {'East Timor': 'tp', 'Canada': 'ca', 'Turkmenistan': 'tm',
                 'Congo, The Democratic Republic Of The': 'cd', 'Kazakstan': 'kz', 'Iran, Islamic Republic Of': 'ir',
                 'Cambodia': 'kh', 'Switzerland': 'ch', 'Ethiopia': 'et', 'Aruba': 'aw', 'Swaziland': 'sz',
                 'Argentina': 'ar', 'Bolivia': 'bo', 'Cameroon': 'cm', 'Burkina Faso': 'bf', 'Ghana': 'gh',
                 'Saudi Arabia': 'sa', 'Saint Helena': 'sh', 'Togo': 'tg', 'Japan': 'jp', 'American Samoa': 'as',
                 'United States Minor Outlying Islands': 'um', 'Northern Mariana Islands': 'mp', 'Pakistan': 'pk',
                 'Guatemala': 'gt', 'Kuwait': 'kw', 'Russian Federation': 'ru', 'Jordan': 'jo',
                 'Virgin Islands, British': 'vg', 'Dominica': 'dm', 'Liberia': 'lr', 'Maldives': 'mv', 'Jamaica': 'jm',
                 'Lithuania': 'lt', 'Martinique': 'mq', 'Saint Kitts And Nevis': 'kn', 'Svalbard And Jan Mayen': 'sj',
                 'Albania': 'al', 'French Guiana': 'gf', 'Niue': 'nu', 'Monaco': 'mc', 'New Zealand': 'nz',
                 'Yemen': 'ye', 'Andorra': 'ad', 'Greenland': 'gl', 'Samoa': 'ws', 'Macau': 'mo',
                 'Norfolk Island': 'nf', 'United Arab Emirates': 'ae', 'Guam': 'gu', 'Uruguay': 'uy', 'India': 'in',
                 'Azerbaijan': 'az', 'Lesotho': 'ls', 'Western Sahara': 'eh', 'Saint Vincent And The Grenadines': 'vc',
                 'Kenya': 'ke', 'Tajikistan': 'tj', 'Pitcairn': 'pn', 'Turkey': 'tr', 'Afghanistan': 'af',
                 'South Georgia And The South Sandwich Islands': 'gs', 'Bangladesh': 'bd', 'Mauritania': 'mr',
                 'Solomon Islands': 'sb', 'Viet Nam': 'vn', 'Saint Lucia': 'lc', 'San Marino': 'sm',
                 'French Polynesia': 'pf', 'France': 'fr', 'Cocos (keeling) Islands': 'cc',
                 'Syrian Arab Republic': 'sy', 'Bermuda': 'bm', 'Slovakia': 'sk', 'Somalia': 'so', 'Peru': 'pe',
                 'Vanuatu': 'vu', 'Nauru': 'nr', 'Seychelles': 'sc', 'Norway': 'no', 'Malawi': 'mw',
                 'Cook Islands': 'ck', 'Benin': 'bj', 'Wallis And Futuna': 'wf', 'Cuba': 'cu', 'Montenegro': 'me',
                 'Mayotte': 'yt', 'China': 'cn', 'Armenia': 'am', 'Dominican Republic': 'do',
                 'Moldova, Republic Of': 'md', 'Ukraine': 'ua', 'Bahrain': 'bh', 'Tonga': 'to', 'Finland': 'fi',
                 'Libyan Arab Jamahiriya': 'ly', 'Cayman Islands': 'ky', 'Central African Republic': 'cf',
                 'New Caledonia': 'nc', 'Mauritius': 'mu', 'Liechtenstein': 'li', 'Australia': 'au',
                 'Antigua And Barbuda': 'ag', 'Mali': 'ml', 'Sweden': 'se', 'Bulgaria': 'bg', 'United States': 'us',
                 'Romania': 'ro', 'Angola': 'ao', 'French Southern Territories': 'tf', 'Chad': 'td',
                 'South Africa': 'za', 'Tokelau': 'tk', 'Cyprus': 'cy', 'Brunei Darussalam': 'bn', 'Qatar': 'qa',
                 'Virgin Islands, U.s.': 'vi', 'Malaysia': 'my', 'Austria': 'at', 'Mozambique': 'mz', 'Uganda': 'ug',
                 'Hungary': 'hu', 'Niger': 'ne', 'Brazil': 'br', 'Turks And Caicos Islands': 'tc',
                 'Tanzania, United Republic Of': 'tz', 'Faroe Islands': 'fo', 'Guinea': 'gn', 'Panama': 'pa',
                 'Costa Rica': 'cr', 'Luxembourg': 'lu', 'Cape Verde': 'cv', 'Bahamas': 'bs',
                 'Holy See (vatican City State)': 'va', 'Gibraltar': 'gi', 'Ireland': 'ie',
                 "Korea, Democratic People's Republic Of": 'kp', 'Palau': 'pw', 'Nigeria': 'ng', 'Ecuador': 'ec',
                 'Czech Republic': 'cz', 'Belarus': 'by', 'Algeria': 'dz', 'Slovenia': 'si', 'El Salvador': 'sv',
                 'Tuvalu': 'tv', 'Heard Island And Mcdonald Islands': 'hm', 'Marshall Islands': 'mh', 'Chile': 'cl',
                 'Puerto Rico': 'pr', 'Belgium': 'be', 'Kiribati': 'ki', 'Haiti': 'ht', 'Belize': 'bz',
                 'Hong Kong': 'hk', 'Sierra Leone': 'sl', 'Georgia': 'ge', "Lao People's Democratic Republic": 'la',
                 'Oman': 'om', 'Mexico': 'mx', 'Gambia': 'gm', 'Philippines': 'ph', 'Sao Tome And Principe': 'st',
                 'Morocco': 'ma', 'Falkland Islands (malvinas)': 'fk', 'Croatia': 'hr', 'Mongolia': 'mn',
                 'Micronesia, Federated States Of': 'fm', 'Thailand': 'th', 'Namibia': 'na', 'Grenada': 'gd',
                 "Cote D'ivoire": 'ci', 'Iraq': 'iq', 'Portugal': 'pt', 'Estonia': 'ee', 'Kosovo': 'kv',
                 'Saint Pierre And Miquelon': 'pm', 'Equatorial Guinea': 'gq', 'Lebanon': 'lb', 'Uzbekistan': 'uz',
                 'Tunisia': 'tn', 'Djibouti': 'dj', 'Rwanda': 'rw', 'Spain': 'es', 'Colombia': 'co', 'Reunion': 're',
                 'Burundi': 'bi', 'Fiji': 'fj', 'Barbados': 'bb', 'Madagascar': 'mg', 'Italy': 'it', 'Bhutan': 'bt',
                 'Sudan': 'sd', 'Nepal': 'np', 'Malta': 'mt', 'Netherlands': 'nl', 'Bosnia And Herzegovina': 'ba',
                 'Suriname': 'sr', 'Anguilla': 'ai', 'Venezuela': 've', 'Netherlands Antilles': 'an',
                 'United Kingdom': 'gb', 'Christmas Island': 'cx', 'Indonesia': 'id', 'Iceland': 'is', 'Zambia': 'zm',
                 'Korea, Republic Of': 'kr', 'Senegal': 'sn', 'Papua New Guinea': 'pg',
                 'Taiwan, Province Of China': 'tw', 'Zimbabwe': 'zw', 'Germany': 'de', 'Denmark': 'dk', 'Poland': 'pl',
                 'Eritrea': 'er', 'Kyrgyzstan': 'kg', 'British Indian Ocean Territory': 'io', 'Montserrat': 'ms',
                 'Israel': 'il', 'Sri Lanka': 'lk', 'Latvia': 'lv', 'Guyana': 'gy', 'Guadeloupe': 'gp',
                 'Honduras': 'hn', 'Myanmar': 'mm', 'Bouvet Island': 'bv', 'Egypt': 'eg', 'Nicaragua': 'ni',
                 'Singapore': 'sg', 'Serbia': 'rs', 'Botswana': 'bw',
                 'Macedonia, The Former Yugoslav Republic Of': 'mk', 'Trinidad And Tobago': 'tt', 'Antarctica': 'aq',
                 'Congo': 'cg', 'Guinea-bissau': 'gw', 'Greece': 'gr', 'Paraguay': 'py', 'Gabon': 'ga',
                 'Palestinian Territory, Occupied': 'ps', 'Comoros': 'km'}


def country_to_range(country):
    """
    Get and return the ip range for a given country.
    """
    with open('countries/{}.txt'.format(country)) as f:
        return f.read().splitlines()


def create_rule():
    if "-A INPUT -m set --match-set blacklist src -j DROP" not in subprocess.Popen("iptables -S INPUT", shell=True,
                                                                                   stdout=subprocess.PIPE).stdout.read():
        subprocess.Popen("iptables -A INPUT -m set --match-set blacklist src -j DROP", shell=True,
                         stdout=subprocess.PIPE)

    if "-A OUTPUT -m set --match-set blacklist dst -j DROP" not in subprocess.Popen("iptables -S OUTPUT", shell=True,
                                                                                stdout=subprocess.PIPE).stdout.read():
        subprocess.Popen("iptables -A OUTPUT -m set --match-set blacklist dst -j DROP", shell=True,
                         stdout=subprocess.PIPE)


def create_set():
    if "blacklist" not in subprocess.Popen("ipset list | grep Name:", shell=True, stdout=subprocess.PIPE).stdout.read():
        subprocess.Popen("ipset create blacklist nethash", shell=True, stdout=subprocess.PIPE)
    create_rule()


def block(country):
    """
    Given a county, block it from sending packets to this machine.
    """
    create_set()
    ranges = country_to_range(country)
    for rng in ranges:
        subprocess.Popen("ipset add blacklist {}".format(rng), shell=True, stdout=subprocess.PIPE)


def unblock(country):
    """
    Given a county, unblock it from sending packets to this machine.
    """
    create_set()
    ranges = country_to_range(country)
    for rng in ranges:
        subprocess.Popen("ipset del blacklist {}".format(rng), shell=True, stdout=subprocess.PIPE)


if __name__ == "__main__":
    if os.name != 'posix':
        sys.exit()
    else:
        args = shlex.split(" ".join(sys.argv))
        if len(args) == 3:
            operation = args[1]
            if args[2] in country_to_cc and operation.lower() in ['block', 'unblock']:
                cc = country_to_cc[args[2]]
                if operation.lower() == 'block':
                    block(cc)
                else:
                    unblock(cc)
            else:
                sys.exit()


