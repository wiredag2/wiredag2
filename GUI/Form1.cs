﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using System.Collections.Concurrent;
using System.IO;
using System.Diagnostics;

namespace WireDagForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        ConcurrentQueue<string> cq;
        ConcurrentQueue<int> closeall;
        Dictionary<string, int> countries;
        Thread listener;
        Thread statistics;
        ConcurrentQueue<int> Inbound;
        ConcurrentQueue<int> Outbound;
        ConcurrentQueue<string> countries_q;
        private void Form1_Load(object sender, EventArgs e)
        {
            
            tabControl1.SelectedTab.Text = "Conversations";
            tabControl1.TabPages[1].Text = "Statistics";
            tabControl1.TabPages[2].Text = "Firewall";
            cq = new ConcurrentQueue<string>();
            closeall = new ConcurrentQueue<int>();
            Inbound = new ConcurrentQueue<int>();
            countries_q = new ConcurrentQueue<string>();
            Outbound = new ConcurrentQueue<int>();
            WindowState = FormWindowState.Maximized;
            int width = Width, height = Height;
            tabControl1.Width = width - 40;
            tabControl1.Height = height - 90;
            listView1.Width = width - 60;
            listView1.Height = Height - 60;
            //string[] data = new string[10];
            //for (int i = 0; i < 10; i++) { data[i] = i.ToString(); }
            //procces_list(data);
            //procces_list(data);
            int c_width = (int)(listView1.Width / listView1.Columns.Count);
            foreach (ColumnHeader header in listView1.Columns)
            {
                header.Width = c_width;
            }
            init_file();
            listener = new Thread(createListener);
            statistics = new Thread(stats_handler);
            listener.Start();
            statistics.Start();
            SetText("Starting sniffing...");
        }
        private void init_file()
        {
            string[] lines = File.ReadAllLines("countries.txt");
            IEnumerable<string> extract = from string line in lines select line.Split('|')[1];
            comboBox1.Items.AddRange(extract.ToArray());
        }
        long totalin = 0, totalout = 0;
        private void stats_handler()
        {
            while (closeall.Count<1)
            {
                Thread.Sleep(2);
                int inb = 0, outb = 0;
                if (Inbound.Count > 0)
                {
                    Inbound.TryDequeue(out inb);
                }
                if (Outbound.Count > 0)
                {
                    Outbound.TryDequeue(out outb);
                }
                totalin += inb;
                totalout += outb;
                double div = totalin + totalout;
                if(div!=0)
                {
                    totalin = (int)(totalin / div * 100);
                    totalout = (int)(totalout / div * 100);
                }
                label3.Invoke(new Action(() => { label3.Text = totalin.ToString()+"%"; }));
                label4.Invoke(new Action(() => { label4.Text = totalout.ToString()+"%"; }));
                
            }
        }
        Process process;
        public void createListener()
        {
            ProcessStartInfo start = new ProcessStartInfo();
            if (IsRunningOnMono())
            {
                start.FileName = "/usr/bin/python";
                start.Arguments = "Sniffer.py";
            }
            else
            {
                start.FileName = "C:/Python27/python.exe";
                start.Arguments = "Sniffer.py";
            }
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;
            start.CreateNoWindow = true;
            process = new Process();
            process.EnableRaisingEvents = true;
            process.ErrorDataReceived += error;
            process.OutputDataReceived +=  process_data;
            process.StartInfo = start;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
            process.OutputDataReceived -= process_data;
            process.ErrorDataReceived -= error;
        }
        
        private void error(object sender, DataReceivedEventArgs e)
        {
            //SetText(e.Data);
        }

        public static bool IsRunningOnMono()
        {
            return Type.GetType("Mono.Runtime") != null;
        }
        public void procces_list(string[] data)
        {
            SetText("Sniffing...");
            listView1.Invoke(new Action(() =>
            {

                foreach (ListViewItem item in listView1.Items)
                {
                    if (item.Text == data[0])
                    {
                        for (int i = 1; i < data.Length; i++)
                        {
                            if (data[i] != "")
                            { item.SubItems[i].Text = data[i]; }
                            if ((i == 8) && (data[i] != ""))
                            { item.BackColor = Color.LightSalmon; }
                        }
                        return;
                    }
                }
                ListViewItem lvi = new ListViewItem();
                lvi.Text = data[0];
                lvi.BackColor = Color.LightGreen;
                data = data.Skip(1).ToArray();
                lvi.SubItems.AddRange(data);
                listView1.Items.Add(lvi);
            }));
        }
       
        public void process_data(object sendingProcess, DataReceivedEventArgs outLine)
        {
            if (!string.IsNullOrEmpty(outLine.Data))
            {
                string data = outLine.Data;
                string[] splited_d = data.Split('|');
                string[] disp = new string[9];
                for (int i = 0; i < 9; i++) { disp[i] = ""; }
                disp[0] = splited_d[0];
                switch (splited_d.Length)
                {
                    case 6:
                        disp[1] = splited_d[1];
                        disp[2] = splited_d[2];
                        disp[3] = splited_d[3];
                        disp[4] = splited_d[4];
                        disp[7] = splited_d[5];
                        countries_q.Enqueue(splited_d[2]);
                        countries_q.Enqueue(splited_d[4]);
                        break;
                    case 3:
                        disp[5] = splited_d[1];
                        disp[6] = splited_d[2];
                        Inbound.Enqueue(int.Parse(splited_d[1]));
                        Outbound.Enqueue(int.Parse(splited_d[2]));
                        break;
                    case 2:
                        disp[8] = splited_d[1];
                        break;

                    default:
                        break;
                }
                procces_list(disp);
            }
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            process.Kill();
            closeall.Enqueue(1);
            Thread.Sleep(10);
            Environment.Exit(0);
        }


        delegate void SetTextCallback(string text);
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (Status.GetCurrentParent().InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {                this.Status.Text = text;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (FindListViewItemForMessage(comboBox1.SelectedText) == null)
            {
                System.Diagnostics.Process b_process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.FileName = "/usr/bin/python";
                startInfo.Arguments = "Block.py block " + "\\\"" + comboBox1.Text + "\\\"";
                process.StartInfo = startInfo;
                process.Start();
                listView2.Items.Add(comboBox1.Text);
            }
        }
        //exe
        private void button2_Click(object sender, EventArgs e)
        {

            System.Diagnostics.Process b_process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = "/usr/bin/python";
            startInfo.Arguments = "Block.py unblock " + "\\\"" + comboBox1.Text + "\\\"";
            process.StartInfo = startInfo;
            process.Start();
            DeleteIfNecessary(comboBox1.Text);

        }
        void DeleteIfNecessary(string message)
        {
            ListViewItem listViewItem = FindListViewItemForMessage(message);
            if (listViewItem == null)
            {
                // item doesn't exist
                return;
            }
            this.listView2.Items.Remove(listViewItem);
        }

        private ListViewItem FindListViewItemForMessage(string s)
        {
            foreach (ListViewItem lvi in this.listView2.Items)
            {
                if (StringComparer.OrdinalIgnoreCase.Compare(lvi.Text, s) == 0)
                {
                    return lvi;
                }
            }
            return null;
        }

    }
    class ListViewNF : System.Windows.Forms.ListView
    {
        public ListViewNF()
        {
            //Activate double buffering
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);

            //Enable the OnNotifyMessage event so we get a chance to filter out 
            // Windows messages before they get to the form's WndProc
            this.SetStyle(ControlStyles.EnableNotifyMessage, true);
        }

        protected override void OnNotifyMessage(Message m)
        {
            //Filter out the WM_ERASEBKGND message
            if (m.Msg != 0x14)
            {
                base.OnNotifyMessage(m);
            }
        }
    }
}


 