~SETUP INSTRUCTIONS~
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
  echo "deb http://download.mono-project.com/repo/debian wheezy main" | sudo tee /etc/apt/sources.list.d/mono-xamarin.list
  sudo apt-get update
  apt-get install mono-complete
  apt-get install iptables
  apt-get install ipset
  apt-get install python-pip
  pip install geoip2

~BUILD INSTRUCTIONS~
  xbuild GUI/WireDag2.csproj
  cp GUI/bin/Debug/WindowsApplication.exe WireDAG.exe

~RUN INSTRUCTIONS~
 mono WireDAG.exe
 Wait a few seconds (around 5-10) and then you will see TCP conversations appear on the Conversations tab.